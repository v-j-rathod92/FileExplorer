package com.lim.fileexplorer.utils.provider;

import com.lim.fileexplorer.utils.Futils;
import com.lim.fileexplorer.utils.color.ColorPreference;
import com.lim.fileexplorer.utils.theme.AppTheme;
import com.lim.fileexplorer.utils.theme.AppThemeManagerInterface;

/**
 * Created by Rémi Piotaix <remi.piotaix@gmail.com> on 2016-10-17.
 */
public interface UtilitiesProviderInterface {
    Futils getFutils();

    ColorPreference getColorPreference();

    AppTheme getAppTheme();

    AppThemeManagerInterface getThemeManager();
}
